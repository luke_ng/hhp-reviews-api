// prod.js
module.exports = {
  googleApiKey: process.env.GOOGLE_API_KEY,
  s3BucketName: process.env.S3_BUCKET_NAME,
  mongoURI: process.env.MONGO_URI
};
