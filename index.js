const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const path = require("path");
const PORT = process.env.PORT || 5000;
const keys = require("./config/keys");

mongoose.connect(keys.mongoURI);
require("./models/Place");
require("./models/User");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.get("/", (req, res) => res.render("pages/index_simple"));
app.get("/data_entry", (req, res) =>
  res.render("pages/data_entry", {
    formdata: {
      place_id: "",
      place_name: "",
      review: {
        positive: [{ comment: "", count: "" }],
        negative: [{ comment: "", count: "" }]
      }
    }
  })
);

//Wire up the routes to Express
require("./routes/api")(app);
require("./routes/web")(app);

if (process.env.NODE_ENV !== "production") {
  require("./routes/tools")(app);
}

app.listen(PORT, () => console.log(`Listening on ${PORT}`));
