// This sample uses the Place Autocomplete widget to allow the user to search
// for and select a place. The sample then displays an info window containing
// the place ID and other information about the place that the user has
// selected.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initMap() {
  var map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 1.359939, lng: 103.84004 },
    zoom: 13
  });

  var input = document.getElementById("pac-input");

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo("bounds", map);

  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var infowindow = new google.maps.InfoWindow();
  var infowindowContent = document.getElementById("infowindow-content");
  infowindow.setContent(infowindowContent);
  var marker = new google.maps.Marker({
    map: map
  });
  marker.addListener("click", function() {
    infowindow.open(map, marker);
  });

  autocomplete.addListener("place_changed", function() {
    infowindow.close();
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
    }

    // Set the position of the marker using the place ID and location.
    marker.setPlace({
      placeId: place.place_id,
      location: place.geometry.location
    });
    marker.setVisible(true);

    //Auto-fill some known information
    //console.log("lat = " + place.geometry.location.lat());
    //console.log("lng = " + place.geometry.location.lng());
    $("#google_place_id").val(place.place_id);
    $("#place_name").val(place.name);
    $("#place_lat").val(place.geometry.location.lat());
    $("#place_lng").val(place.geometry.location.lng());
    $("#place_rating").val(place.rating);
    populatePhotosList(place.place_id);

    infowindowContent.children["place-name"].textContent = place.name;
    infowindowContent.children["place-id"].textContent = place.place_id;
    infowindowContent.children["place-address"].textContent =
      place.formatted_address;
    infowindow.open(map, marker);
  });
}

function appendPosComment() {
  //Check the number of elements in #pos_cmts first
  var new_index = $(".pos_cmt_row").length;
  $("#pos_cmts").append(
    '<div class="form-row pos_cmt_row"><div class="col-9">' +
      '<input type="text" class="form-control" name="pcmt-' +
      new_index +
      '" placeholder="Enter comment"></div>' +
      '<div class="col"><input type="number" class="form-control" name="pcount-' +
      new_index +
      '" placeholder="Count e.g. 10"></div>' +
      "</div>"
  );
}

function appendNegComment() {
  //Check the number of elements in #pos_cmts first
  var new_index = $(".neg_cmt_row").length;
  $("#neg_cmts").append(
    '<div class="form-row neg_cmt_row"><div class="col-9">' +
      '<input type="text" class="form-control" name="ncmt-' +
      new_index +
      '" placeholder="Enter comment"></div>' +
      '<div class="col"><input type="number" class="form-control" name="ncount-' +
      new_index +
      '" placeholder="Count e.g. 10"></div>' +
      "</div>"
  );
}

function populatePhotosList(place_id) {
  //Make a AJAX request back to the server to get the photo URLS
  $.getJSON("/web/getplacephotos/" + place_id, function(data) {
    var sel_options = [];
    $.each(data.photos, function(index, val) {
      sel_options.push(
        "<option value='" + val + "'>Photo " + (index + 1) + "</option>"
      );
    });

    $("#place_cover_photo")
      .empty()
      .append(sel_options.join(""));

    var all_photos = data.photos.join(";");
    $("#photo_list").val(all_photos);

    loadSelectedPhoto();
  });
}

function loadSelectedPhoto() {
  var selected_photo = $("#place_cover_photo")
    .find(":selected")
    .val();
  $("#photo_preview").attr("src", selected_photo);
}

$(document).ready(function() {
  $("#btn_add_pos_cmt").click(function() {
    appendPosComment();
  });
  $("#btn_add_neg_cmt").click(function() {
    appendNegComment();
  });
  $("#place_cover_photo").change(loadSelectedPhoto);

  $("#btn_loadphotos").click(function() {
    var place_id = $("#google_place_id").val();
    populatePhotosList(place_id);
  });
});
