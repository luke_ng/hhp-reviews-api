//placephotos.js
//THIS IS DEPRECATED! DO NOT USE!!
const mongoose = require("mongoose");
const axios = require("axios");
var AWS = require("aws-sdk");
const keys = require("../config/keys.js");
const Place = mongoose.model("places");

if (process.env.NODE_ENV !== "production") {
  //In dev mode, read local json file for credentials
  AWS.config.loadFromPath("config/dev-aws.json");
  console.log("[placephotos.js] AWS credentials loaded from json file");
}

function uploadImageToS3(place_id, photo_ref, photo_index) {
  var photo_format = "jpg";
  // Create S3 service object
  s3 = new AWS.S3({ apiVersion: "2006-03-01" });
  var uploadParams = {
    Bucket: keys.s3BucketName,
    ACL: "public-read",
    Key: `${place_id}/${photo_index}.${photo_format}`,
    Body: ""
  };
  var file = process.argv[3];
}

var downloadPlaceUrl = photo_ref => {
  return new Promise(async (resolve, reject) => {
    //Make GET request to Google API for image link
    //The API will respond with a 302 redirect, but we
    //do not need to follow th redirect. Just store the
    //redirect location, which will be the public URL of the photo
    axios
      .get("https://maps.googleapis.com/maps/api/place/photo", {
        params: {
          maxwidth: 600,
          photoreference: photo_ref,
          key: keys.googleApiKey
        },
        //responseType: "arraybuffer",
        responseType: "text",
        maxRedirects: 0,
        validateStatus: function(status) {
          return status >= 200 && status <= 302;
        }
      })
      .then(function(response) {
        //console.log(`Response header: ${JSON.stringify(response.headers)}`);
        if (response.status == 302 && response.headers.location) {
          resolve(response.headers.location);
        } else {
          resolve("");
        }
        //var imgtype = response.headers["content-type"];
        //var imgbuffer = new Buffer(response.data, "binary");
        //resolve({ img_type: imgtype, img_buffer: imgbuffer });
      });
  });
};
/*===== Exported functions ======*/

module.exports = {
  downloadPlaceUrl: downloadPlaceUrl,

  getSinglePhotoUrl: function(in_place_id, in_photo_ref) {
    return new Promise(async (resolve, reject) => {
      //Check first if place exists in DB before creating
      var photo_url;
      const existingPlace = await Place.findOne({ place_id: in_place_id });
      if (existingPlace) {
        //Get the first photo
        if (existingPlace.photos.length > 0) {
          photo_url = existingPlace.photos[0];
          console.log("Found URL");
        }

        //If reference not found, download it
        if (!photo_url) {
          console.log("Place in DB but URL not found, need to query...");
          photo_url = await downloadPlaceUrl(in_photo_ref);
          if (existingPlace.photos.length > 0) {
            existingPlace.photos[0] = photo_url;
          } else {
            existingPlace.photos.push(photo_url);
          }
          await existingPlace.save();
        }
        resolve(photo_url);
      } else {
        //New Place entry needs to be created
        //Photo needs to be downloaded
        console.log("Place not in DB.. Downloading photo..");
        photo_url = await downloadPlaceUrl(in_photo_ref);

        newplace = await new Place({
          place_id: in_place_id,
          photos: [photo_url]
        }).save();
        console.log(`New place saved: ${newplace}`);
        resolve(photo_url);
      }
    });
  }
};
