//googleapi.js
const axios = require("axios");
const keys = require("../config/keys.js");

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/*===== Exported functions ======*/

module.exports = {
  getNearbyPlaces: function(
    in_lat,
    in_long,
    val_radius,
    val_keyword,
    val_rankby
  ) {
    return axios.get(
      "https://maps.googleapis.com/maps/api/place/nearbysearch/json",
      {
        params: {
          type: "restaurant",
          location: `${in_lat}, ${in_long}`,
          radius: val_radius,
          keyword: val_keyword,
          rankby: val_rankby,
          key: keys.googleApiKey
        }
      }
    );
  },

  getNearbyPlacesNextPage: async function(in_pagetoken) {
    await timeout(1500);
    return axios.get(
      "https://maps.googleapis.com/maps/api/place/nearbysearch/json",
      {
        params: {
          pagetoken: in_pagetoken,
          key: keys.googleApiKey
        }
      }
    );
  },

  getPlaceDetail: function(in_place_id) {
    return axios.get(
      "https://maps.googleapis.com/maps/api/place/details/json?",
      {
        params: {
          placeid: in_place_id,
          key: keys.googleApiKey
        }
      }
    );
  },

  getLocationCoords: function(place_name) {
    return axios.get(
      "https://maps.googleapis.com/maps/api/geocode/json?",
      {
        params: {
          address: place_name,
          region: "sg",
          key: keys.googleApiKey
        }
      }
    );
  },

  downloadPlaceUrl: function(photo_ref) {
    return new Promise(async (resolve, reject) => {
      //Make GET request to Google API for image link
      //The API will respond with a 302 redirect, but we
      //do not need to follow th redirect. Just store the
      //redirect location, which will be the public URL of the photo
      axios
        .get("https://maps.googleapis.com/maps/api/place/photo", {
          params: {
            maxwidth: 600,
            photoreference: photo_ref,
            key: keys.googleApiKey
          },
          //responseType: "arraybuffer",
          responseType: "text",
          maxRedirects: 0,
          validateStatus: function(status) {
            return status >= 200 && status <= 302;
          }
        })
        .then(function(response) {
          //console.log(`Response header: ${JSON.stringify(response.headers)}`);
          if (response.status == 302 && response.headers.location) {
            resolve(response.headers.location);
          } else {
            resolve("");
          }
          //var imgtype = response.headers["content-type"];
          //var imgbuffer = new Buffer(response.data, "binary");
          //resolve({ img_type: imgtype, img_buffer: imgbuffer });
        });
    });
  }
};
