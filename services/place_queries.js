//place_queries.js
const mongoose = require("mongoose");
const Place = mongoose.model("places");
const googleApi = require("./googleapi");

function getNearbyPlaces(in_lat, in_lng, radius, search_page) {
  return new Promise(async (resolve, reject) => {
    //search_page starts from 1
    var page = search_page < 1 ? 0 : search_page - 1;
    //console.log(`getNearbyPlaces page: ${page}`);
    var query = Place.find({
      place_name: { $exists: true }
    })
      .where("location")
      .near({
        center: [in_lng, in_lat],
        maxDistance: radius / 6378100,
        spherical: true
      })
      .skip(page * 10)
      .limit(10)
      //.select({ place_id: 1, place_name: 1, photos: 1, review: 1 })
      .slice("review.positive", 1) //Return only the top positive comment
      .slice("review.negative", 1); //Similarly for neg comment

    query.exec(function(err, result) {
      if (err) reject(err);
      resolve(result);
    });
  });
}

/*===== Exported functions ======*/

module.exports = {
  getNearbyPlaces: getNearbyPlaces,

  getPlacesByGoogleId: function(place_ids) {
    return new Promise(async (resolve, reject) => {
      var query = Place.find({
        place_id: { $in: place_ids },
        place_name: { $exists: true }
      })
        .select({ place_id: 1, place_name: 1, photos: 1, review: 1 })
        .slice("review.positive", 1)
        .slice("review.negative", 1);

      query.exec(function(err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  },

  //Input radius here is in meters.
  //The correct radius value will be converted in the function
  getAddressCoords: function(address, radius, search_page) {
    return new Promise(async (resolve, reject) => {
      //Translate the address to geo coordinates
      let geocode_resp = await googleApi.getLocationCoords(address);

      //If status= "ZERO_RESULTS" or results[] is empty, call reject
      if (geocode_resp.data.status) {
        if (geocode_resp.data.status == "ZERO_RESULTS"){
          //console.log("calling reject: Invalid address");
          reject("Invalid address");
        }
      }
      if (geocode_resp.data.results.length == 0) {
        //console.log("calling reject: Invalid address");
        reject("Invalid address");
      }

      //let addr_lat = geocode_resp.results[0].geometry.location.lat;
      //let addr_lng = geocode_resp.results[0].geometry.location.lng;
      resolve(geocode_resp.data.results[0].geometry.location);
    });
  },

  //Gets all Photo URLs of a place
  getPlacePhotos: function(place_id) {
    return new Promise(async (resolve, reject) => {
      var photo_promises = [];
      var photo_urls = [];
      try {
        var placeDetails = await googleApi.getPlaceDetail(place_id);
        for (photo_meta of placeDetails.data.result.photos) {
          //console.log(photo_meta.photo_reference);
          photo_promises.push(
            googleApi.downloadPlaceUrl(photo_meta.photo_reference)
          );
        }
      } catch (error) {
        console.log("Error getting place details: " + error);
      }

      if (photo_promises.length > 0) {
        Promise.all(photo_promises)
          .then(photo_urls => {
            //console.log("All promise returned...");
            //console.log(photo_urls);
            resolve(photo_urls);
          })
          .catch(reason => {
            console.log(`Error: ${reason}`);
            reject(`Error: ${reason}`);
          });
      } else {
        resolve(photo_urls);
      }
    });
  }
};
