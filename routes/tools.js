//tools.js
const keys = require("../config/keys");
const mongoose = require("mongoose");
const Place = mongoose.model("places");
const User = mongoose.model("users");
const googleapi = require("../services/googleapi");
const place_queries = require("../services/place_queries");
const html_enti = require("he");

module.exports = app => {
  app.get("/tools/db_clear_invalid_data", async (req, res) => {
    //Remove those entries without place_name
    Place.remove({ place_name: { $exists: false } }, function(err) {
      if (err) return res.send(err);
      // removed!
      res.send("Invalid data cleared");
    });
  });

  app.get("/tools/fill_missing_info", (req, res) => {
    //Check if we have places with missing info and fill in with data from
    //Google Places API
    var query = Place.find({
      $or: [{ location: { $exists: false } }, { rating: { $exists: false } }]
    });
    query.exec(async function(err, result) {
      if (err) return res.send(err);
      //For each place_id, get the info we need and update
      for (place of result) {
        console.log(`Searching for Place ID ${place.place_id}`);
        gdata = await googleapi.getPlaceDetail(place.place_id);
        if (gdata.data.result) {
          //Update the coordinates
          place.location = [
            gdata.data.result.geometry.location.lng,
            gdata.data.result.geometry.location.lat
          ];
          //Update the rating
          place.rating = gdata.data.result.rating;
          place.last_updated = Date.now();
          await place.save();
        }
      }
      res.send("Fill Missing Info OK");
    });
  });

  app.get("/tools/fill_missing_photo", async (req, res) => {
    var query = Place.find({
      photos: { $size: 0 }
    });
    query.exec(async function(err, result) {
      if (err) return res.send(err);
      //For each place_id, get the info we need and update
      for (place of result) {
        console.log(`Searching for Place ID ${place.place_id}`);
        gdata = await googleapi.getPlaceDetail(place.place_id);
        if (gdata.data.result && gdata.data.result.photos) {
          //Get the photo reference first
          let photoref = gdata.data.result.photos[0].photo_reference;
          let photo_url = await googleapi.downloadPlaceUrl(photoref);
          console.log(`Photo URL obtained ${photo_url}`);
          //Update the photo
          place.photos = [photo_url];
          place.last_updated = Date.now();
          await place.save();
        }
      }
      res.send("Fill Missing Photos OK");
    });
  });

  // For decoding HTML entities found in strings
  app.get("/tools/decode_he", async (req, res) => {
    var query = Place.find({
      place_name: { $exists: true }
    });
    query.exec(async function(err, result) {
      if (err) return res.send(err);
      //For each place_id, run through the HTML entity decoder and
      //compare. if same as original then no need to update
      for (place of result) {
        console.log(`Looking at Place ${place.place_id}`);
        let modified = false;
        let dec_place_name = html_enti.decode(place.place_name);
        if (dec_place_name !== place.place_name) {
          place.place_name = dec_place_name;
          modified = true;
        }

        for (let i = 0; i < place.review.positive.length; i++) {
          let dec_rev = html_enti.decode(place.review.positive[i].comment);
          if (dec_rev !== place.review.positive[i].comment) {
            place.review.positive[i].comment = dec_rev;
            modified = true;
          }
        }

        for (let i = 0; i < place.review.negative.length; i++) {
          let dec_rev = html_enti.decode(place.review.negative[i].comment);
          if (dec_rev !== place.review.negative[i].comment) {
            place.review.negative[i].comment = dec_rev;
            modified = true;
          }
        }

        if (modified) {
          console.log(`Place will be updated ${place.place_id}`);
          place.last_updated = Date.now();
          await place.save();
        }
      }
      res.send("Decode HTML entities OK");
    });
  });

  // For adding all place photos in entries with single cover photo
  app.get("/tools/fill_missing_photos", async (req, res) => {
    var query = Place.find({
      photos: { $size: 1 }
    });
    query.exec(async function(err, result) {
      if (err) return res.send(err);

      for (place of result) {
        console.log(`Looking at Place ${place.place_id}`);
        let all_photos = await place_queries.getPlacePhotos(place.place_id);
        //Remove cover_photo from photo_list, so we don't have repeat when
        //we combine them.
        let cover_photo = place.photos[0];
        if (all_photos.length > 1) {
          let index_to_del = -1;
          for (let i = 0; i < all_photos.length; i++) {
            if (all_photos[i] === cover_photo) {
              index_to_del = i;
              break;
            }
          }
          if (index_to_del >= 0) {
            all_photos.splice(index_to_del, 1);
            //Add cover photo to beginning of array
            all_photos.unshift(cover_photo);
          }
          console.log(`Place will be updated ${place.place_id}`);
          place.photos = all_photos;
          place.last_updated = Date.now();
          await place.save();
        } else {
          //No need to update
          console.log("Not updating.. only 1 or 0 photo for place");
        }
      }

      res.send("Fill missing photos OK");
    });
  });

  // For printing all places urls for editing
  app.get("/tools/place_edit_urls", async (req, res) => {
    var query = Place.find({
      place_name: { $exists: true }
    });
    query.exec(async function(err, result) {
      if (err) return res.send(err);
      let domain = "https://rocky-forest-32974.herokuapp.com";
      let api = "/web/editplace/";
      for (place of result) {
        console.log(domain + api + place.place_id);
      }

      res.send("Print place edit URLS OK");
    });
  });
};
