//api.js
const axios = require("axios");
const keys = require("../config/keys");
const placeQueries = require("../services/place_queries");
const fs = require("fs");
const mongoose = require("mongoose");
const Place = mongoose.model("places");
const User = mongoose.model("users");

const { check, validationResult } = require("express-validator/check");
const { sanitizeBody } = require("express-validator/filter");

function package_response(places_result) {
  /*
  Format each result in the following manner
  result_format = {
    location: {
      lat: 0,
      lng: 0
    },
    name: "",
    photo_url: "",
    place_id: "",
    rating: 0,
    vicinity: "",
    review: {}
  }
  */
  var response_pkt = {
    status: "",
    results: []
  };

  for (plc of places_result) {
    let outresult = {};
    outresult.location = { lat: plc.location[1], lng: plc.location[0] };
    outresult.name = plc.place_name;
    outresult.place_id = plc.place_id;
    outresult.rating = plc.rating;
    outresult.vicinity = plc.vicinity;

    if (plc.photos && plc.photos.length > 0) {
      outresult.photo_url = plc.photos[0];
    }
    outresult.review = {
      positive: plc.review.positive[0],
      negative: plc.review.negative[0]
    };

    response_pkt.results.push(outresult);
  }

  if (places_result.length < 1) {
    response_pkt.status = "NO_RESULTS_FOUND";
  } else {
    response_pkt.status = "OK";
  }

  return response_pkt;
}


module.exports = app => {
  app.get("/api/hello", async (req, res) => {
    res.send("Hello! It works!");
  });

  app.get("/api/nearby_food", async (req, res) => {
    //Required parameters: Lat, Long
    let in_lat = req.query.lat;
    if (!in_lat) {
      return res
        .status(400)
        .send({ error: "Location info is necessary! (Latitude)" });
    }

    let in_long = req.query.lng;
    if (!in_long) {
      return res
        .status(400)
        .send({ error: "Location info is necessary! (Longitude)" });
    }

    //Optional parameters: radius, keyword, user PSID
    let val_radius = "2000";
    if (req.query.radius) {
      val_radius = req.query.radius;
    }

    let search_page = 1;
    if (req.query.search_page) {
      search_page = req.query.search_page;
    }

    if (req.query.psid) {
      let in_psid = req.query.psid;
      // Save the search parameters for this user
      const existingUser = await User.findOne({
        psid: in_psid
      });
      let search_params = {
        lat: in_lat,
        lng: in_long,
        radius: val_radius,
        search_page: search_page
      };
      if (existingUser) {
        existingUser.search_hist = search_params;
        existingUser.last_activity = Date.now();
        await existingUser.save();
      } else {
        var newuser = new User({
          psid: in_psid,
          usage_count: 0,
          search_hist: search_params
        });
        await newuser.save();
      }
    }

    var response_pkt = {
      status: "",
      results: []
    };

    try {
      var places_result = await placeQueries.getNearbyPlaces(
        in_lat,
        in_long,
        val_radius,
        search_page
      );
    } catch (db_error) {
      //console.log(error)
      response_pkt.status = `DB_ERROR ${db_error}`;
      return res.status(500).send(response_pkt);
    }

    response_pkt = package_response(places_result);
    res.send(response_pkt);
  });


  app.get("/api/nearby_food_by_address", async (req, res) => {
    //Required parameters: address
    let in_addr = req.query.address;
    if (!in_addr) {
      return res
        .status(400)
        .send({ error: "Address is necessary!" });
    }

    try{
      var location = await placeQueries.getAddressCoords(in_addr);
      //console.log(location);
    } catch(err){
      return res
        .status(400)
        .send({ error: err });
    }

    //Optional parameters: radius, keyword, user PSID
    let val_radius = "2000";
    if (req.query.radius) {
      val_radius = req.query.radius;
    }

    let search_page = 1;
    if (req.query.search_page) {
      search_page = req.query.search_page;
    }

    if (req.query.psid) {
      let in_psid = req.query.psid;
      // Save the search parameters for this user
      const existingUser = await User.findOne({
        psid: in_psid
      });
      let search_params = {
        lat: location.lat,
        lng: location.lng,
        radius: val_radius,
        search_page: search_page
      };
      if (existingUser) {
        existingUser.search_hist = search_params;
        existingUser.last_activity = Date.now();
        await existingUser.save();
      } else {
        var newuser = new User({
          psid: in_psid,
          usage_count: 0,
          search_hist: search_params
        });
        await newuser.save();
      }
    }

    var response_pkt = {
      status: "",
      results: []
    };

    try {
      var places_result = await placeQueries.getNearbyPlaces(
        location.lat,
        location.lng,
        val_radius,
        search_page
      );
    } catch (db_error) {
      //console.log(error)
      response_pkt.status = `DB_ERROR ${db_error}`;
      return res.status(500).send(response_pkt);
    }

    response_pkt = package_response(places_result);
    res.send(response_pkt);
  });


  app.get("/api/place/:placeId", async (req, res) => {
    let placeId = req.params.placeId;
    if (!placeId) {
      return res.status(400).send({ error: "Place ID is necessary!" });
    }

    const existingPlace = await Place.findOne({
      place_id: placeId
    });

    if (existingPlace) {
      //var responsePkt = {};
      res.send(existingPlace);
    } else {
      return res.status(404).send();
    }
  });

  app.post(
    "/api/user/record_usage",
    [
      //Required input: FB user's PSID
      // Validate that the required fields are not empty.
      check("fb_psid", "Facebook PSID required")
        .isLength({ min: 1 })
        .trim(),
      // Sanitize (trim and escape) the fields (using wildcard).
      sanitizeBody("*")
        .trim()
        .escape()
    ],
    async (req, res) => {
      // Extract the validation errors from a request.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        // There are errors. Send error messages.
        let errArr = errors.array({ onlyFirstError: true });
        return res.status(400).send({ error: errArr[0].msg });
      } else {
        let in_psid = req.body.fb_psid;
        let in_name = "";
        if (req.body.user_name) {
          in_name = req.body.user_name;
        }
        var retPayload = { usage_count: 0, name: "" };
        // Check if User with same ID already exists.
        const existingUser = await User.findOne({
          psid: in_psid
        });
        if (existingUser) {
          //Do update
          existingUser.usage_count += 1;
          existingUser.last_activity = Date.now();
          if (in_name) {
            existingUser.name = in_name;
          }
          await existingUser.save();
          retPayload.usage_count = existingUser.usage_count;
          retPayload.name = existingUser.name;
        } else {
          var newuser = new User({
            psid: in_psid,
            name: in_name,
            usage_count: 1
          });
          await newuser.save();
          retPayload.usage_count = newuser.usage_count;
          retPayload.name = newuser.name;
        }

        return res.send(retPayload);
      }
    }
  );

  app.post(
    "/api/user/record_activity",
    [
      //Required input: FB user's PSID
      // Validate that the required fields are not empty.
      check("fb_psid", "Facebook PSID required")
        .isLength({ min: 1 })
        .trim(),
      // Sanitize (trim and escape) the fields (using wildcard).
      sanitizeBody("*")
        .trim()
        .escape()
    ],
    async (req, res) => {
      // Extract the validation errors from a request.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        // There are errors. Send error messages.
        let errArr = errors.array({ onlyFirstError: true });
        return res.status(400).send({ error: errArr[0].msg });
      } else {
        //Check optional parameters
        let in_visited_place = "";
        if (req.body.visited_place) {
          in_visited_place = req.body.visited_place;
        }

        let in_psid = req.body.fb_psid;
        // Check if User with same ID already exists.
        const existingUser = await User.findOne({
          psid: in_psid
        });
        if (existingUser) {
          //Do update
          if (in_visited_place) {
            existingUser.last_visited_place = in_visited_place;
          }
          existingUser.last_activity = Date.now();
          await existingUser.save();
        } else {
          return res.status(404).send();
        }
        return res.send("OK");
      }
    }
  );

  app.get("/api/user/:psid", async (req, res) => {
    let in_psid = req.params.psid;
    if (!in_psid) {
      return res.status(400).send({ error: "User PSID is necessary!" });
    }

    const existingUser = await User.findOne({
      psid: in_psid
    });

    if (existingUser) {
      //var responsePkt = {};
      res.send(existingUser);
    } else {
      return res.status(404).send();
    }
  });

  app.get("/api/user/:psid/last_search", async (req, res) => {
    let in_psid = req.params.psid;
    if (!in_psid) {
      return res.status(400).send({ error: "User PSID is necessary!" });
    }

    const existingUser = await User.findOne({
      psid: in_psid
    });

    if (existingUser) {
      try {
        var places_result = await placeQueries.getNearbyPlaces(
          existingUser.search_hist.lat,
          existingUser.search_hist.lng,
          existingUser.search_hist.radius,
          existingUser.search_hist.search_page
        );

        var response_pkt = {
          status: "",
          results: []
        };

        for (plc of places_result) {
          let outresult = {};
          outresult.location = { lat: plc.location[1], lng: plc.location[0] };
          outresult.name = plc.place_name;
          outresult.place_id = plc.place_id;
          outresult.rating = plc.rating;
          outresult.vicinity = plc.vicinity;

          if (plc.photos && plc.photos.length > 0) {
            outresult.photo_url = plc.photos[0];
          }
          outresult.review = {
            positive: plc.review.positive[0],
            negative: plc.review.negative[0]
          };

          response_pkt.results.push(outresult);
        }

        if (places_result.length < 1) {
          response_pkt.status = "NO_RESULTS_FOUND";
        } else {
          response_pkt.status = "OK";
        }

        //Update user activity tracking
        existingUser.last_activity = Date.now();
        await existingUser.save();

        res.send(response_pkt);
      } catch (db_error) {
        //console.log(error)
        response_pkt.status = `DB_ERROR ${db_error}`;
        return res.status(500).send(response_pkt);
      }
    } else {
      return res.status(404).send();
    }
  });

  app.get("/api/user/:psid/last_search/next_results", async (req, res) => {
    let in_psid = req.params.psid;
    if (!in_psid) {
      return res.status(400).send({ error: "User PSID is necessary!" });
    }

    const existingUser = await User.findOne({
      psid: in_psid
    });

    if (existingUser) {
      try {
        let next_search_pg = existingUser.search_hist.search_page + 1;
        var places_result = await placeQueries.getNearbyPlaces(
          existingUser.search_hist.lat,
          existingUser.search_hist.lng,
          existingUser.search_hist.radius,
          next_search_pg
        );

        var response_pkt = {
          status: "",
          results: []
        };

        for (plc of places_result) {
          let outresult = {};
          outresult.location = { lat: plc.location[1], lng: plc.location[0] };
          outresult.name = plc.place_name;
          outresult.place_id = plc.place_id;
          outresult.rating = plc.rating;
          outresult.vicinity = plc.vicinity;

          if (plc.photos && plc.photos.length > 0) {
            outresult.photo_url = plc.photos[0];
          }
          outresult.review = {
            positive: plc.review.positive[0],
            negative: plc.review.negative[0]
          };

          response_pkt.results.push(outresult);
        }

        if (places_result.length < 1) {
          response_pkt.status = "NO_RESULTS_FOUND";
        } else {
          response_pkt.status = "OK";
          //Update the results page
          existingUser.search_hist.search_page = next_search_pg;
        }

        //Update user activity tracking
        existingUser.last_activity = Date.now();
        await existingUser.save();

        res.send(response_pkt);
      } catch (db_error) {
        //console.log(error)
        response_pkt.status = `DB_ERROR ${db_error}`;
        return res.status(500).send(response_pkt);
      }
    } else {
      return res.status(404).send();
    }
  });
};
