//web.js
const mongoose = require("mongoose");
const Place = mongoose.model("places");
const googleApi = require("../services/googleapi");
const place_queries = require("../services/place_queries");

const { check, validationResult } = require("express-validator/check");
const { sanitizeBody } = require("express-validator/filter");
const html_enti = require("he");

module.exports = app => {
  app.post(
    "/web/post_review",
    [
      // Validate that the required fields are not empty.
      check("google_place_id", "Google Place ID required")
        .isLength({ min: 1 })
        .trim(),
      check("place_lat", "Latitude required")
        .isLength({ min: 1 })
        .trim(),
      check("place_lng", "Longitude required")
        .isLength({ min: 1 })
        .trim(),
      check("place_name", "Place name required")
        .isLength({ min: 1 })
        .trim(),

      // Sanitize (trim and escape) the fields (using wildcard).
      sanitizeBody("*")
        .trim()
        .escape()
    ],

    // Process request after validation and sanitization.
    async (req, res) => {
      // Extract the validation errors from a request.
      const errors = validationResult(req);

      //Extract the positive comments
      var positive = [];
      var i = 0;
      while (i < 50) {
        //check for pcmt-i & pcount-i
        if (req.body["pcmt-" + i]) {
          //console.log("Comment received: " + req.body["pcmt-" + i]);
          //console.log("Count: " + req.body["pcount-" + i]);
          positive.push({
            comment: html_enti.decode(req.body["pcmt-" + i]),
            count: req.body["pcount-" + i]
          });
          i += 1;
        } else {
          break;
        }
      }

      //Extract the negative comments
      var negative = [];
      i = 0;
      while (i < 50) {
        //check for ncmt-i & ncount-i
        if (req.body["ncmt-" + i]) {
          //console.log("Comment received: " + req.body["ncmt-" + i]);
          //console.log("Count: " + req.body["ncount-" + i]);
          negative.push({
            comment: html_enti.decode(req.body["ncmt-" + i]),
            count: req.body["ncount-" + i]
          });
          i += 1;
        } else {
          break;
        }
      }

      //Get the rating (Might not be there)
      var place_rating = 0;
      if (req.body.place_rating) {
        place_rating = html_enti.decode(req.body.place_rating);
      }

      var in_place = new Place({
        place_id: req.body.google_place_id,
        place_name: html_enti.decode(req.body.place_name),
        rating: place_rating,
        review: {
          positive: positive,
          negative: negative
        },
        location: [req.body.place_lng, req.body.place_lat]
      });

      //Extract the list of photos
      if (req.body.photo_list) {
        let cleaned_input = req.body.photo_list.replace(/&#x2F;/g, "/");
        let photo_list = cleaned_input.split(";");

        if (req.body.place_cover_photo) {
          let cover_photo = req.body.place_cover_photo.replace(/&#x2F;/g, "/");
          //console.log("Place photo: " + dec_url);
          //Remove cover_photo from photo_list, so we don't have repeat when
          //we combine them.
          if (photo_list.length > 1) {
            let index_to_del = -1;
            for (let i = 0; i < photo_list.length; i++) {
              if (photo_list[i] === cover_photo) {
                index_to_del = i;
                break;
              }
            }
            if (index_to_del >= 0) {
              photo_list.splice(index_to_del, 1);
              //Add cover photo to beginning of array
              photo_list.unshift(cover_photo);
            }
            in_place.photos = photo_list;
          } else {
            in_place.photos = [cover_photo];
          }
        } else {
          //No cover photo received, so just save photo_list
          in_place.photos = photo_list;
        }
      } else if (req.body.place_cover_photo) {
        let dec_url = req.body.place_cover_photo.replace(/&#x2F;/g, "/");
        //console.log("Place photo: " + dec_url);
        in_place.photos = [dec_url];
      }

      if (!errors.isEmpty()) {
        // There are errors. Render the form again with sanitized values/error messages.
        res.render("pages/data_entry", {
          formdata: in_place,
          errors: errors.array()
        });
        return;
      } else {
        // Data from form is valid.
        // Check if Place with same ID already exists.
        const existingPlace = await Place.findOne({
          place_id: in_place.place_id
        });

        if (existingPlace) {
          //Do update
          existingPlace.place_name = in_place.place_name;
          existingPlace.rating = in_place.rating;
          existingPlace.review = in_place.review;
          if (in_place.photos) {
            existingPlace.photos = in_place.photos;
          }
          existingPlace.last_updated = Date.now();
          await existingPlace.save();
        } else {
          await in_place.save();
        }

        res.render("pages/post_review_success");
      }
    }
  );

  // Return a JSON array of URLs of photos belonging to a place
  app.get("/web/getplacephotos/:placeId", async (req, res) => {
    var response_pkt = { photos: [] };

    try {
      response_pkt.photos = await place_queries.getPlacePhotos(
        req.params.placeId
      );
      return res.send(response_pkt);
    } catch (error) {
      console.log(error);
      res.status(500).send({
        error: `Error in Promise.all Getting all photo URLs: ${error}`
      });
    }
  });

  app.get("/web/editplace/:placeId", async (req, res) => {
    let placeId = req.params.placeId;
    if (!placeId) {
      return res.status(400).send({ error: "Place ID is necessary!" });
    }

    var place_data = await Place.findOne({ place_id: placeId });
    if (place_data) {
      res.render("pages/data_entry", {
        formdata: place_data
      });
    } else {
      return res.status(404).send({ error: "Place not found!" });
    }
  });

  app.get("/web/showplace/:placeId", async (req, res) => {
    let placeId = req.params.placeId;
    if (!placeId) {
      return res.status(400).send({ error: "Place ID is necessary!" });
    }

    var place_data = await Place.findOne({ place_id: placeId });
    if (place_data) {
      res.render("pages/place_comments", {
        pagedata: place_data
      });
    } else {
      return res.status(404).send({ error: "Place not found!" });
    }
  });
};
