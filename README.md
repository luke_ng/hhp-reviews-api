## REST API

**Title :** Search nearby food data

**URL :** https://rocky-forest-32974.herokuapp.com/api/nearby_food

**Method :** GET

**Query Params :**

lat - GPS coordinate of search point (Latitude)

lng - GPS coordinate of search point (Longitude)

psid - (Optional) Facebook user PSID. Adding this will save the search history of that user

search_page - (Optional) If this value is not supplied, backend will assume search_page=1, returning the first page of results. Each set of results returned are limited to 10. If no more results, response JSON will have status: NO_RESULTS_FOUND

Sample request: https://rocky-forest-32974.herokuapp.com/api/nearby_food_sample?lat=1.23445&lng=103.23442

**Response Codes :** Success (200 OK)

**Example responses**

Successful with results returned

```
#!json
(200 OK)
{
  "status":"OK",
  "results":[
    {
      "location":{"lat":1.392054,"lng":103.8954022},
      "name":"KFC",
      "photo_url":"https://lh3.googleusercontent.com/p/AF1QipPcRMPo0UdegZYU36dHBmPCzJN_9iE_C6oVDkw=s1600-w600",
      "place_id":"ChIJJ4D-iA0W2jERaP9rlTiJ-Zo",
      "rating":3.6,
      "review": {
        "positive": {
          "comment": "Great chicken",
          "count": 48
        },
        "negative": {
          "comment": "Overpriced",
          "count": 3
        }
      }
    },
    {
      "location":{"lat":1.392054,"lng":103.8954022},
      "name":"SAKAE SUSHI",
      "photo_url":"https://lh3.googleusercontent.com/p/AF1QipPZO56VQY1iYuB2-4-E-yK6MqWnoq5LsTZ5fY-m=s1600-w600",
      "place_id":"ChIJS0DshQ0W2jERx66HWWBAh10",
      "rating":2.3,
      "review": {
        "positive": {
          "comment": "Cheap food",
          "count": 18
        },
        "negative": {
          "comment": "Bad service",
          "count": 28
        }
      }
    },
    {
      "location":{"lat":1.392054,"lng":103.8954022},
      "name":"Big Bites Pure Vegetarian Restaurant",
      "photo_url":"https://lh3.googleusercontent.com/p/AF1QipNNRoj9KoJLPVrWTmYvf0SXPOmdQVKNAHEMRoh2=s1600-w600",
      "place_id":"ChIJDWZChBIW2jERU4YdCyRsku4",
      "rating":5,
      "review": {
        "positive": {
          "comment": "Great food",
          "count": 23
        },
        "negative": {
          "comment": "Queue is long (Can be up to 1 hr)",
          "count": 13
        }
      }
    }
  ]
}
```

Successful, but no more places within search radius

```
#!json
(No results found)
{
  "status":"NO_RESULTS_FOUND",
  "results":[]
}
```

---

**Title :** Get Place details

**URL :** https://rocky-forest-32974.herokuapp.com/api/place/<PLACE_ID>

**Method :** GET

**Query Params :**

<PLACE_ID> - Google ID of place

Sample request: https://rocky-forest-32974.herokuapp.com/api/place/ChIJd_7DgE8W2jERPk9MvW9ZRNc

**Response Codes :** Success (200 OK), Not Found (404)

**Example responses**

```
#!json
(200 OK)
{
  "place_id":"ChIJd_7DgE8W2jERPk9MvW9ZRNc",
  "place_name":"Divine Realm Vegetarian Restaurant",
  "photos":[],
  "review": {
    "positive":[ {"comment":"Nice food","count":23},
                 {"comment":"Reasonable price","count":16},
                 {"comment":"Good Ramen","count":7},
                 {"comment":"Good variety","count":6},
                 {"comment":"Prata","count":2},
                 {"comment":"Clean environment","count":2},
                 {"comment":"Now has aircon","count":2}
              ],
    "negative":[ {"comment":"Weekend and dinner time is crowded","count":3},
                 {"comment":"Poor services","count":3},
                 {"comment":"Insufficient food portion","count":2}
              ]
  },
  "last_updated":"2018-01-21T11:14:29.216Z",
  "_id":"5a6476156c0301000488249e",
  "__v":0
}
```

---

**Title :** Record chatbot usage

**Description :** Use this endpoint to track a new user, or increment the usage count of an existing user. Optionally, the user's name can be stored. The user name can be changed by including the new name in the request.

**URL :** https://rocky-forest-32974.herokuapp.com/api/user/record_usage

**Method :** POST

**POST Params :**

fb_psid - PSID of Facebook user

(Optional) user_name - Name of Facebook user

**Response Codes :** Success (200 OK), Missing Parameter Error (400)

**Response Data**

The response will contain the statistics of the user's usage count AFTER the database update. So if a new user has been entered, the response will have a usage count of 1.

**Example responses**

```
#!json
(200 OK)
{
    "usage_count": 2,
    "name": "Squall Leonhart"
}
```

---

**Title :** Record user activity

**Description :** Use this endpoint to update the "last_activity" timestamp of a user. Optionally, it can also be used to record the name of the place which the user visited.

**URL :** https://rocky-forest-32974.herokuapp.com/api/user/record_activity

**Method :** POST

**POST Params :**

fb_psid - PSID of Facebook user

(Optional) visited_place - To record the name of the place which the user last visited

**Response Codes :** Success (200 OK), Missing Parameter Error (400), User not found (404)

**Response Data**

If successfully updated the last_activity timestamp, the response will be "OK"

---

**Title :** Get User details

**URL :** https://rocky-forest-32974.herokuapp.com/api/user/<PS_ID>

**Method :** GET

**Query Params :**

<PS_ID> - PSID of Facebook user

Sample request: https://rocky-forest-32974.herokuapp.com/api/user/test1234

**Response Codes :** Success (200 OK), Not Found (404)

**Example responses**

```
#!json
(200 OK)
{
    "psid":"test1234",
    "name":"",
    "usage_count":1,
    "last_visited_place":"Divine Realm Vegetarian",
    "last_activity":"2018-01-30T13:15:11.205Z",
    "_id":"5a706fdfba882c0004424574",
    "__v":0
}
```

---

**Title :** Get User's Previous Search Results

**URL :** https://rocky-forest-32974.herokuapp.com/api/user/<PS_ID>/last_search

**Method :** GET

**Query Params :**

<PS_ID> - PSID of Facebook user

Sample request: https://rocky-forest-32974.herokuapp.com/api/user/test1234/last_search

**Response Codes :** Success (200 OK), Not Found (404)

---

**Title :** Get Next Page of User's Previous Search Results

**URL :** https://rocky-forest-32974.herokuapp.com/api/user/<PS_ID>/last_search/next_results

**Method :** GET

**Query Params :**

<PS_ID> - PSID of Facebook user

Sample request: https://rocky-forest-32974.herokuapp.com/api/user/test1234/last_search/next_results

**Response Codes :** Success (200 OK), Not Found (404)

If no more results, response JSON will have status: NO_RESULTS_FOUND

## Web Views

**Place Comments**

**URL :** https://rocky-forest-32974.herokuapp.com/web/showplace/<PLACE_ID>

## More Info

For more information about using Node.js on Heroku, see these Dev Center articles:

* [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
* [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
* [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
* [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
* [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)
