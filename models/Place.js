//Place.js

const mongoose = require("mongoose");
const { Schema } = mongoose;
//Equivalent to: const Schema = mongoose.Schema;

const placeSchema = new Schema({
  place_id: String,
  place_name: String,
  photos: [String],
  review: {
    positive: [{ comment: String, count: Number }],
    negative: [{ comment: String, count: Number }]
  },
  //Mongo stores [Longitude,Latitude]
  location: { type: [Number], index: "2dsphere" },
  rating: Number,
  last_updated: { type: Date, default: Date.now }
});

mongoose.model("places", placeSchema);
