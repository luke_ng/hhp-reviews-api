//User.js

const mongoose = require("mongoose");
const { Schema } = mongoose;
//Equivalent to: const Schema = mongoose.Schema;

const userSchema = new Schema({
  psid: String,
  name: String,
  usage_count: Number,
  search_hist: {
    lat: Number,
    lng: Number,
    radius: Number,
    search_page: Number
  },
  last_visited_place: String,
  last_activity: { type: Date, default: Date.now }
});

mongoose.model("users", userSchema);
